# Latihan Git #

1. Bikin akun di gitlab.com dan github.com
2. Buat local repo
3. Commit pertama ke local repo
4. Commit sebagian dengan menggunakan staging area
5. Berbagai model undo
6. Push ke multiple remote repo
7. Membuat branch
8. Merge branch
9. Push branch ke remote repo

# Latihan Kolaborasi #
1. Menambahkan member ke repo kita
2. Membuat perubahan di lokasi berbeda
3. Membuat perubahan di lokasi yang sama

# Workflow #
1. Feature branch
2. Release Management